#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/generate.h>
#include <iostream>
#include <thrust/copy.h>
#include <thrust/random.h>
#include <cuda.h>

#include "count.cuh"
using namespace std;

int main(int argc, char *argv[])
{
  int n = std::stoi(argv[1]);
  thrust::host_vector<int> h_vec(n);

  srand(time(0));
  //printf("hvec\n");
  for(int i = 0 ; i < n ; i++)
  {
    h_vec[i] = static_cast <int> (rand()) / static_cast <int> (RAND_MAX/500);
    //printf("%d\t", h_vec[i]);
  }
  //printf("\n");

  thrust::device_vector<int> d_vec(n);
  thrust::device_vector<int> values;
  thrust::device_vector<int> counts;

  thrust::copy(h_vec.begin(), h_vec.end(), d_vec.begin());

  cudaEvent_t start, end;
  cudaEventCreate(&start);
  cudaEventCreate(&end);
  //start timer
  cudaEventRecord(start, 0);
  count(d_vec, values, counts);
  cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

  cudaEventSynchronize(end);
  float timeTaken;
  cudaEventElapsedTime(&timeTaken, start, end);

  cudaDeviceSynchronize();

  thrust::host_vector<int> h_val(n);
  thrust::host_vector<int> h_count(n);

  thrust::copy(values.begin(), values.end(), h_val.begin());
  thrust::copy(counts.begin(), counts.end(), h_count.begin());

  std::cout << h_val[values.size()-1] << "\n";        //print last elements
  std::cout << h_count[values.size()-1] << "\n" ;
  std::cout << timeTaken << "\n";                       //print time taken

  // printf("values\n");
  // for(int i = 0 ; i < values.size() ; i++)
  // {
  //   std::cout << h_val[i] << "\t";
  // }
  // printf("\n");
  //
  // printf("counts\n");
  // for(int i = 0 ; i < counts.size() ; i++)
  // {
  //   std::cout << h_count[i] << "\t";
  // }
  // printf("\n");

}
