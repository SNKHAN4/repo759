#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/generate.h>
#include <thrust/unique.h>
#include <thrust/execution_policy.h>
#include <iostream>
#include <thrust/copy.h>
#include <thrust/random.h>
#include <cuda.h>

#include "count.cuh"

void count(const thrust::device_vector<int>& d_in,
                 thrust::device_vector<int>& values,
                 thrust::device_vector<int>& counts)
{
  thrust::device_vector<int> d_vec(d_in.size());
  thrust::copy(d_in.begin(), d_in.end(), d_vec.begin());  //input array is constant so cant be modified
  thrust::sort(d_vec.begin(), d_vec.end());     //sort data

  thrust::device_vector<int> d_vec_unique(d_vec.size());
  thrust::copy(d_vec.begin(), d_vec.end(), d_vec_unique.begin());  //input array is constant so cant be modified

  int new_end = thrust::unique(d_vec_unique.begin(), d_vec_unique.end()) - d_vec_unique.begin();     //unique, only modifies the first few elements until you run out of unique elements, after that the rest of the array is preserved

  thrust::device_vector<int> constvec(d_in.size(), 1);
  values.resize(new_end);
  counts.resize(new_end);
  thrust::reduce_by_key(d_vec.begin(), d_vec.end(), constvec.begin(), values.begin(), counts.begin());  //input sorted array as A, array with all 1s as B, store results and counts

}
