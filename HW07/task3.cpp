#include <stdio.h>
#include <omp.h>
#include <iostream>

int main(int argc, char *argv[]) {
    omp_set_num_threads(4);
    #pragma omp parallel
        {
            if(omp_get_thread_num() == 0)
              printf("Number of threads: %d\n", omp_get_num_threads()); // Print number of threads only once
            printf("I am thread No. %d\n", omp_get_thread_num()); // Print thread number
        }

    #pragma omp parallel for
        for (int i = 1; i < 9; i++) {
            int result = 1;         //re-initialize result to one after every iteration
            for(int j = 1 ; j <= i ; j++)
              result = result*j;          //calculate factorial here

            printf("%d!=%d\n", i, result); // Print factorial after inner loop is finished
        }
    return 0;
}
