#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/generate.h>
#include <iostream>
#include <thrust/copy.h>
#include <thrust/random.h>
#include <cuda.h>

int main(int argc , char *argv[])
{
  int n = std::stoi(argv[1]);   //read command line argument
  float reduce_op;

  srand(time(0));
  thrust::host_vector<float> h_vec(n);        //allotting thrust host vector of size n
  for(int i = 0 ; i < n ; i++)
  {
    h_vec[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;   //assign random values between -1 and 1
    //printf("%f\t", h_vec[i]);
  }

  thrust::device_vector<float> d_vec= h_vec;    //copy host vector data to device vector

  cudaEvent_t start, end;
  cudaEventCreate(&start);
  cudaEventCreate(&end);
  //start timer
  cudaEventRecord(start, 0);
  reduce_op = thrust::reduce(d_vec.begin(), d_vec.end());     //store result of thrust operation into reduce_op
  cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

  cudaEventSynchronize(end);
  float timeTaken;
  cudaEventElapsedTime(&timeTaken, start, end);

  cudaDeviceSynchronize();

  printf( "%f\n", reduce_op);   //print results
  printf("%f\n", timeTaken);

  return 0;
}
