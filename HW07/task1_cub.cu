#define CUB_STDERR // print CUDA runtime errors to console
#include <stdio.h>
#include <cub/util_allocator.cuh>
#include <cub/device/device_reduce.cuh>
#include "cub/util_debug.cuh"
#include <cuda.h>


using namespace cub;
CachingDeviceAllocator  g_allocator(true);  // Caching allocator for device memory

int main(int argc , char *argv[]) {
    const size_t num_items = std::stoi(argv[1]);
    // Set up host arrays
    float *h_in = (float*) malloc(num_items*sizeof(float));

    srand(time(0));
    for(int i = 0 ; i < num_items ; i++)
    {
      h_in[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;    //assign random values between -1 and 1
      //printf("%f\t", h_in[i]);
    }

    // Set up device arrays
    float* d_in = NULL;
    CubDebugExit(g_allocator.DeviceAllocate((void**)& d_in, sizeof(float) * num_items));
    // Initialize device input
    CubDebugExit(cudaMemcpy(d_in, h_in, sizeof(float) * num_items, cudaMemcpyHostToDevice));
    // Setup device output array
    float* d_sum = NULL;
    CubDebugExit(g_allocator.DeviceAllocate((void**)& d_sum, sizeof(float) * 1));
    // Request and allocate temporary storage
    void* d_temp_storage = NULL;
    size_t temp_storage_bytes = 0;
    CubDebugExit(DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items));
    CubDebugExit(g_allocator.DeviceAllocate(&d_temp_storage, temp_storage_bytes));

    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);
    //start timer
    cudaEventRecord(start, 0);
    // Do the actual reduce operation
    DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, num_items);  //actual reduce operation
    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);

    cudaDeviceSynchronize();

    float gpu_sum;
    CubDebugExit(cudaMemcpy(&gpu_sum, d_sum, sizeof(float) * 1, cudaMemcpyDeviceToHost));
    // Check for correctness
    //printf("\t%s\n", (gpu_sum == sum ? "Test passed." : "Test falied."));
    printf("%f\n", gpu_sum);
    printf("%f\n", timeTaken);

    // Cleanup
    if (d_in) CubDebugExit(g_allocator.DeviceFree(d_in));
    if (d_sum) CubDebugExit(g_allocator.DeviceFree(d_sum));
    if (d_temp_storage) CubDebugExit(g_allocator.DeviceFree(d_temp_storage));

    return 0;
}
