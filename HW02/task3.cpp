#include<iostream>
#include"matmul.h"
#include<ctime>


#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{

	high_resolution_clock::time_point start, end;
	duration<double , std::milli> duration_sec;

	srand(time(0));
	int n = 1001;
  cout << n << endl;
	double *A = (double*) malloc(n*n * sizeof(double));
	double *B = (double*) malloc(n*n * sizeof(double));
  double *C = (double*) malloc(n*n * sizeof(double));

  std::vector<double> A1(n*n) ;
  std::vector<double> B1(n*n) ;

	for(int i = 0 ; i < n*n ; i ++)
	{
		A[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;
    A1[i] = A[i];
    B[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;
    B1[i] = B[i];
  }

  for(int i  = 0 ; i < n*n ; i++)
      C[i] = 0;
	start = high_resolution_clock::now();
	mmul1(A, B,C, n);
	end = high_resolution_clock::now();


	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout <<duration_sec.count() <<  endl  ;
  cout << C[n*n-1] << endl;
  for(int i  = 0 ; i < n*n ; i++)
      C[i] = 0;
  start = high_resolution_clock::now();
	mmul2(A, B,C, n);
	end = high_resolution_clock::now();

	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout << duration_sec.count() << endl ;
  cout << C[n*n-1] << endl;
  for(int i  = 0 ; i < n*n ; i++)
      C[i] = 0;
  start = high_resolution_clock::now();
	mmul3(A, B,C, n);
	end = high_resolution_clock::now();

	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout <<duration_sec.count() <<  endl  ;
  cout << C[n*n-1] << endl;
  for(int i  = 0 ; i < n*n ; i++)
      C[i] = 0;
  start = high_resolution_clock::now();
	mmul4(A1, B1,C, n);
	end = high_resolution_clock::now();

	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout <<duration_sec.count() <<  endl  ;
  cout << C[n*n-1] << endl;
	free(A);
  free(B);
	free(C);

    // float input[16] = { 1,3,4,8,6,5,2,4,3,4,6,8,1,4,5,2};
    //
  	// float mask[9] = { 0,0,1,0,1,0,1,0,0};
  	// float output[16] = {};
  	// convolve(input, output, 4 ,mask, 3 );
    //
    // cout << "" << output[0] << endl;
    // cout << "" << output[15] << endl;

	return 0;
}
