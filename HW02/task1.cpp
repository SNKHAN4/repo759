#include<iostream>
#include"scan.h"
#include<ctime>


#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{

	high_resolution_clock::time_point start, end;
	duration<double , std::milli> duration_sec;

	srand(time(0));
	long int N = std::stoi(argv[1]);


	float *array = (float*) malloc(N * sizeof(float));
	float *output = (float*) malloc(N* sizeof(float));

	for(int i = 0 ; i < N ; i ++)
	{
		//array[i] = float( 1+ (ran);
		array[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5;
		//cout << array[i] << endl;
	}

	start = high_resolution_clock::now();
	scan(array, output, N);
	end = high_resolution_clock::now();

	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout << duration_sec.count() << endl;
	cout << output[0] << endl;
	cout << output[N-1] << endl;

	free(array);
	free(output);
	return 0;
}
