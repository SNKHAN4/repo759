#include<iostream>
#include"matmul.h"


using namespace std;
void mmul1(const double* A, const double* B, double* C, const unsigned int n)
{
  for(std::size_t i = 0 ; i < n ; i++)
  {
    for(std::size_t j = 0 ; j < n ; j++)
    {
      for(std::size_t k = 0 ; k < n ; k++)
      {
        C[n*i + j] += A[n*i +k] * B[n*k + j];
        //cout << "i:" << i << "\tj:" << j << "\tk" << k << "\top:" << C[n*i + j]  << endl;
      }
      //cout << C[n*i + j] << "\t";
    }
    //cout << endl;
  }
}

void mmul2(const double* A, const double* B, double* C, const unsigned int n)
{
  for(std::size_t i = 0 ; i < n ; i++)
  {
    for(std::size_t k = 0 ; k < n ; k++)
    {
      for(std::size_t j = 0 ; j < n ; j++)
      {
        C[n*i + j] += A[n*i +k] * B[n*k + j];
      }
    }
  }

  // for(int i = 0 ; i < n ; i++)
  // {
  //   for(int j = 0 ; j < n ; j++)
  //   {
  //     cout<< C[n*i + j] << "\t";
  //   }
  //   cout << endl;
  // }
}

void mmul3(const double* A, const double* B, double* C, const unsigned int n)
{
  for(std::size_t j = 0 ; j < n ; j++)
  {
    for(std::size_t k = 0 ; k < n ; k++)
    {
      for(std::size_t i = 0 ; i < n ; i++)
      {
        C[n*i + j] += A[n*i +k] * B[n*k + j];
      }

    }

  }
  // for(int i = 0 ; i < n ; i++)
  // {
  //   for(int j = 0 ; j < n ; j++)
  //   {
  //     cout<< C[n*i + j] << "\t";
  //   }
  //   cout << endl;
  // }
}

void mmul4(const std::vector<double>& A, const std::vector<double>& B, double* C, const unsigned int n)
{
  for(std::size_t i = 0 ; i < n ; i++)
  {
    for(std::size_t j = 0 ; j < n ; j++)
    {
      for(std::size_t k = 0 ; k < n ; k++)
      {
        C[n*i + j] += A[n*i +k] * B[n*k + j];
        //cout << "i:" << i << "\tj:" << j << "\tk" << k << "\top:"<< C[n*i + j]  << endl;
      }
      //cout << C[n*i + j] << "\t";
    }
    //cout << endl;
  }
}

// int main()
// {
//   std::vector<double> A = { 1,2,3,4};
//   std::vector<double> B = { 1,2,3,4};
//
//   double C[4];
//   double A1[4] =  { 1,2,3,4};
//   double B1[4] =  { 1,2,3,4};
//
//   // mmul1(A1, B1,C, 2);
//   // for(int i  = 0 ; i < 4 ; i++)
//   //   C[i] = 0;
//   // mmul2(A1, B1,C, 2);
//   // for(int i  = 0 ; i < 4 ; i++)
//   //   C[i] = 0;
//   // mmul3(A1, B1,C, 2);
//   for(int i  = 0 ; i < 4 ; i++)
//      C[i] = 0;
//
//   mmul4(A, B,C, 2);
//   // for(int i = 0 ; i < 4 ; i++)
//   //   cout << C[i] << endl;
//   //
//   // for(int i  = 0 ; i < 4 ; i++)
//   //   C[i] = 0;
//   // mmul1(A1, B1,C, 2);
//   // for(int i = 0 ; i < 4 ; i++)
//   //   cout << C[i] << endl;
//
//   //
//   // 	float mask[9] = { 0,0,1,0,1,0,1,0,0};
//   // 	float output[16] = {};
//   // 	convolve(input, output, 4 ,mask, 3 );
//   // 	return 0;
// }
