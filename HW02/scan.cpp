#include<iostream>
#include<string>
#include"scan.h"


using namespace std;


float findsum(const float *arr , std::size_t n)
{
	float sum = 0.0;
	for( std::size_t  i = 0 ; i<=n ;i ++)
	{
		sum+=arr[i];
	}
	return sum;
}

void scan(const float *arr, float *output, std::size_t n)
{
	
	output[0] = arr[0];
	for( std::size_t i = 1; i < n ; i++)
	{
		output[i] = output[i-1] + arr[i];
		//cout << "sum of " << i << "elements =" <<output[i] << endl;
	}
}

/*
int main(int argc, char *argv[])
{
	//cout << argv[1] << endl;
	int N = std::stoi(argv[1]);
	float array[N], output[N];
	for(int i = 0 ; i < N ; i++)
	{
		array[i] = float(i);
		cout << array[i] << endl;
	}

	float array2[5] = {1.0, 1.0 , 1.0 ,1.0, 1.0};
	scan(array2 , output, 5);
	return 0;
}
*/
