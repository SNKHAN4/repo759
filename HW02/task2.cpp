#include<iostream>
#include"convolution.h"
#include<ctime>


#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{

	high_resolution_clock::time_point start, end;
	duration<double , std::milli> duration_sec;

	srand(time(0));
	long int n = std::stoi(argv[1]);
  long int m = std::stoi(argv[2]);


	float* image = new float[n*n];
	float* mask = new float[m*m];
        float* output = new float[n*n]; 

	for(int i = 0 ; i < n*n ; i ++)
	{
		//array[i] = float( 1+ (ran);
		image[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/20) - 10;
		
    if(i < m*m)
    {
      mask[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5;
      //cout << "mask" << mask[i] << endl;
    }
    //cout << "image"<< image[i] << endl;
	}


	start = high_resolution_clock::now();
	convolve(image, output, n , mask, m );
	end = high_resolution_clock::now();


	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
	cout << duration_sec.count() << endl;
	cout << output[0] << endl;
	cout << output[n*n-1] << endl;

	delete[] image;
  delete[] mask;
	delete[] output;

    // float input[16] = { 1,3,4,8,6,5,2,4,3,4,6,8,1,4,5,2};
    //
  	// float mask[9] = { 0,0,1,0,1,0,1,0,0};
  	// float output[16] = {};
  	// convolve(input, output, 4 ,mask, 3 );
    //
    // cout << "" << output[0] << endl;
    // cout << "" << output[15] << endl;

	return 0;
}
