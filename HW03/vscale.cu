#include <cuda.h>
#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "vscale.cuh"

__global__ void vscale(const float *a, float *b, unsigned int n)
{
    unsigned int index = 512*blockIdx.x  + threadIdx.x; //converting to do 1D math
    if(index < n)     //if n is not a multiple of 512, out of bounds accesses will take place for index > n
    {
      b[index] = a[index] * b[index];
      //std::printf("thread:%d = %f\n",index,  b[index]);
    }


}
