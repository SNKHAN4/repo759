#include <cuda.h>
#include <stdio.h>
#include <cstdio>

//function to calculate factorial in gpu
__global__ void factorial()
{
    int i = threadIdx.x+1;  //threadId starts from 0, so adding 1
    int result = i;

    while(i > 1)
    {
      result = result * (i-1);  //calculating factorial
      i--;
    }

    std::printf("%d!=%d\n", threadIdx.x + 1, result);
}

int main()
{
  const int numThreads = 8;
  const int numBlocks = 1;
  factorial<<<numBlocks,numThreads>>>();  //run this function in gpu with 1 block and 8 threads. no arguments passed
  cudaDeviceSynchronize();  //synchronization instruction until all threads finish execution
  return 0;
}
