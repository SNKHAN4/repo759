#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "vscale.cuh"

int main(int argc , char *argv[])
{
  if(argc==2)
  {

    long int n = std::stoi(argv[1]);
    const int numThreads = 512;
    int numBlocks;

    if(n<=numThreads)
        numBlocks = 1;
    else
        numBlocks = n/numThreads + (n%numThreads != 0); //if n%numthreads = 0 means condition is false, so returns 0, in all other cases an extra block is allotted

  //allocate host arrays on device
    float *hA = (float*) malloc(n*sizeof(float));
    float *hB = (float*) malloc(n*sizeof(float));
    float *hC = (float*) malloc(n*sizeof(float));

  //allocate memory for dA on device and set all elements to 0, referred to setArray.cu in https://github.com/DanNegrut/759-2022
    float *dA = NULL;
    float *dB = NULL;

    cudaMalloc((void **)&dA , n*sizeof(float));
    cudaMalloc((void **)&dB , n*sizeof(float));

    srand(time(0)); //to give different seed for each run to generate random variables
    for(unsigned i = 0 ; i < n ; i++)
    {

      hA[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/20) - 10;  //generate random numbers from 0 to 20, then subtract with 10  to get -10 to 10
      hB[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) ;    //generate random numbers from 0 to 1
    }

    cudaMemcpy(dA, hA, n*sizeof(float) , cudaMemcpyHostToDevice);   //copy host data to device
    cudaMemcpy(dB, hB, n*sizeof(float) , cudaMemcpyHostToDevice);

    //timing setup taken from lecture 10 slides
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    cudaEventRecord(start, 0);

    vscale<<<numBlocks,numThreads>>>(dA, dB, n);  //execute this on gpu

    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu
    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);

    cudaDeviceSynchronize();
    cudaMemcpy(hC, dB, n*sizeof(float), cudaMemcpyDeviceToHost);  //copy results from device to host
    std::printf("%f\n", timeTaken); //print elapsed timeTaken

    cudaEventDestroy(start);
    cudaEventDestroy(end);


    std::printf("%f\n", hC[0]);   //print first element of result array
    std::printf("%f\n", hC[n-1]); //print last element of result array

    cudaFree(dA);     //final memory cleanup
    cudaFree(dB);

    free(hA);
    free(hB);
    free(hC);
    return 0;
  }
  else
  {
    std::printf("Enter number of elements(n) as a command line argument\nExiting ...:\n");
    return 0;
  }
}
