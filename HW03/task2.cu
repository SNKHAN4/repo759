#include <cuda.h>
#include <stdio.h>
#include <cstdio>


//function to do computation in gpu
__global__ void linear_eq(int a , int *d)
{
  d[8*blockIdx.x + threadIdx.x] = a*threadIdx.x + blockIdx.x; //8 = numThreads
}

int main()
{
  const int numThreads = 8;
  const int numBlocks = 2;
  const int numElems = 16;

  int hA[numElems], *dA;  //host and device arrays

  //allocate memory for dA on device and set all elements to 0, referred to setArray.cu in https://github.com/DanNegrut/759-2022
  cudaMalloc((void **)&dA, sizeof(int)*numElems);
  cudaMemset(dA , 0 , numElems * sizeof(int));

  //generating a random number from 0 to 10
  srand(time(0));
  int a = static_cast <double> (rand()) / static_cast <double> (RAND_MAX/10);
  //printf("%d\n",a);
  linear_eq<<<numBlocks,numThreads>>>(a, dA);  //run this function in gpu with 2 block and 8 threads. dA=data array and a = random integer

  cudaMemcpy(&hA, dA, sizeof(int) * numElems, cudaMemcpyDeviceToHost); //copying the array from device to host array

  for(int i = 0 ; i < numElems ; i++)
    printf("%d ", hA[i]);
  cudaDeviceSynchronize();
  return 0;
}
