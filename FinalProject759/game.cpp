#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <limits.h>
#include <pthread.h>
#include <omp.h>
#include "board.h"
#include "game.h"




typedef struct thread_obj* thread_t;

struct thread {
  int current_depth;
  int maximum_depth;
  int alpha;
  int beta;
  board B;
  int current_player;
  minimaxResult_t result;
};

void gen_moves_king(board B, int x, int y,
    std::vector<move_t>* possibleMoves, int current_player) 
    {
    // king can move in any direction
    move_t m1, m2, m3, m4, m5, m6, m7, m8;
    m1 = (move_t) malloc(sizeof(struct move));
    m2 = (move_t) malloc(sizeof(struct move));
    m3 = (move_t) malloc(sizeof(struct move));
    m4 = (move_t) malloc(sizeof(struct move));
    m5 = (move_t) malloc(sizeof(struct move));
    m6 = (move_t) malloc(sizeof(struct move));
    m7 = (move_t) malloc(sizeof(struct move));
    m8 = (move_t) malloc(sizeof(struct move));
    m1->x1 = x;
    m1->y1 = y;
    m2->x1 = x;
    m2->y1 = y;
    m3->x1 = x;
    m3->y1 = y;
    m4->x1 = x;
    m4->y1 = y;
    m5->x1 = x;
    m5->y1 = y;
    m6->x1 = x;
    m6->y1 = y;
    m7->x1 = x;
    m7->y1 = y;
    m8->x1 = x;
    m8->y1 = y;

    m1->x2 = x + 1;   //king only moves one square at time, generating all possible moves. 
    m1->y2 = y;

    m2->x2 = x + 1;
    m2->y2 = y - 1;

    m3->x2 = x + 1;
    m3->y2 = y + 1;

    m4->x2 = x;
    m4->y2 = y - 1;

    m5->x2 = x;
    m5->y2 = y + 1;

    m6->x2 = x - 1;
    m6->y2 = y;

    m7->x2 = x - 1;
    m7->y2 = y - 1;

    m8->x2 = x - 1;
    m8->y2 = y + 1;

    piece p = get_piece_info(B, x,y );
      //checking how many out of the 8 moves are legal, if legal enter those moves into the vector
    if (check_legal_move(p, B, m1->x2,m1->y2,current_player)) { 
      (*possibleMoves).push_back(m1);                     //if it is a legal move, push that move into the vector at the end
    } else {
      free(m1);
    }
    if (check_legal_move(p, B, m2->x2,m2->y2, current_player)){
      (*possibleMoves).push_back(m2);
    } else {
      free(m2);
    }
    if (check_legal_move(p, B, m3->x2,m3->y2, current_player)) {
      (*possibleMoves).push_back(m3);
    } else {
      free(m3);
    }
    if (check_legal_move(p, B, m4->x2,m4->y2, current_player)) {
      (*possibleMoves).push_back(m4);
    } else {
      free(m4);
    }
    if (check_legal_move(p, B, m5->x2,m5->y2, current_player)) {
      (*possibleMoves).push_back(m5);
    } else {
      free(m5);
    }
    if (check_legal_move(p, B, m6->x2,m6->y2, current_player)) {
      (*possibleMoves).push_back(m6);
    } else {
      free(m6);
    }
    if (check_legal_move(p, B, m7->x2,m7->y2, current_player)) {
      (*possibleMoves).push_back(m7);
    } else {
      free(m7);
    }
    if (check_legal_move(p, B, m8->x2,m8->y2, current_player)) {
      (*possibleMoves).push_back(m8);
    } else {
      free(m8);
    }
  }

  void gen_moves_queen(board B, int x, int y,
      std::vector<move_t>* possibleMoves, int current_player) {
         piece p = get_piece_info(B, x,y );
          //check all possible moves, queen can go anywhere, whether pieces are there are target position or not is checked in check_legal_move
    for (int row = 0; row < BOARD_HEIGHT; row++) {
      for (int col = 0; col < BOARD_HEIGHT; col++) {
        move_t m1 = (move_t)malloc(sizeof(struct move));
        m1 = (move_t) malloc(sizeof(struct move));
        m1->x1 = x;
        m1->y1 = y;
        m1->x2 = col;
        m1->y2 = row;
        if (check_legal_move(p, B, m1->x2, m1->y2, current_player)) {
          (*possibleMoves).push_back(m1);
        } else {
          free(m1);
        }
      }
    }
  }

  void gen_moves_bishop(board B, int x, int y,
      std::vector<move_t>* possibleMoves, int current_player) 
      {
      piece p = get_piece_info(B, x,y );
    for (int row = 0; row < BOARD_HEIGHT; row++) {
      for (int col = 0; col < BOARD_HEIGHT; col++) {
        if (row - col == y - x || (row + col) == x + y) {   //checking diagonal moves
          move_t m1 = (move_t)malloc(sizeof(struct move));
          m1 = (move_t) malloc(sizeof(struct move));
          m1->x1 = x;
          m1->y1 = y;
          m1->x2 = col;
          m1->y2 = row;
          if (check_legal_move(p, B, m1->x2, m1->y2, current_player)) { //if legal move, push into possible moves
            (*possibleMoves).push_back(m1);
          } else {
            free(m1);
          }
        }
      }
    }
}

void gen_moves_knight(board B, int x, int y,
    std::vector<move_t>* possibleMoves, int current_player) {
  // x == x1, y == y1
  move_t m1, m2, m3, m4, m5, m6, m7, m8;
  m1 = (move_t)malloc(sizeof(struct move));
  m2 = (move_t)malloc(sizeof(struct move));
  m3 = (move_t)malloc(sizeof(struct move));
  m4 = (move_t)malloc(sizeof(struct move));
  m5 = (move_t)malloc(sizeof(struct move));
  m6 = (move_t)malloc(sizeof(struct move));
  m7 = (move_t)malloc(sizeof(struct move));
  m8 = (move_t)malloc(sizeof(struct move));
  m1->x1 = x;
  m1->y1 = y;
  m2->x1 = x;
  m2->y1 = y;
  m3->x1 = x;
  m3->y1 = y;
  m4->x1 = x;
  m4->y1 = y;
  m5->x1 = x;
  m5->y1 = y;
  m6->x1 = x;
  m6->y1 = y;
  m7->x1 = x;
  m7->y1 = y;
  m8->x1 = x;
  m8->y1 = y;

  m1->x2 = x + 2;       //L moves for knight
  m1->y2 = y - 1;

  m2->x2 = x + 2;
  m2->y2 = y + 1;

  m3->x2 = x - 2;
  m3->y2 = y - 1;

  m4->x2 = x - 2;
  m4->y2 = y + 1;

  m5->x2 = x + 1;
  m5->y2 = y - 2;

  m6->x2 = x - 1;
  m6->y2 = y - 2;

  m7->x2 = x + 1;
  m7->y2 = y + 2;

  m8->x2 = x - 1;
  m8->y2 = y + 2;
  piece p = get_piece_info(B,x,y);
  if (check_legal_move(p , B, m1->x2, m1->y2,  current_player)) {  
    (*possibleMoves).push_back(m1);
  } else {
    free(m1);
  }
  if (check_legal_move(p, B, m2->x2, m2->y2, current_player)) {
    (*possibleMoves).push_back(m2);
  } else {
    free(m2);
  }
  if (check_legal_move(p, B, m3->x2, m3->y2, current_player)) {
    (*possibleMoves).push_back(m3);
  } else {
    free(m3);
  }
  if (check_legal_move(p, B, m4->x2, m4->y2, current_player)) {
    (*possibleMoves).push_back(m4);
  } else {
    free(m4);
  }
  if (check_legal_move(p, B, m5->x2, m5->y2, current_player)) {
    (*possibleMoves).push_back(m5);
  } else {
    free(m5);
  }
  if (check_legal_move(p, B, m6->x2, m6->y2, current_player)) {
    (*possibleMoves).push_back(m6);
  } else {
    free(m6);
  }
  if (check_legal_move(p, B, m7->x2, m7->y2, current_player)) {
    (*possibleMoves).push_back(m7);
  } else {
    free(m7);
  }
  if (check_legal_move(p, B, m8->x2, m8->y2, current_player)) {
    (*possibleMoves).push_back(m8);
  } else {
    free(m8);
  }

}

void gen_moves_rook(board B, int x, int y,
    std::vector<move_t>* possibleMoves, int current_player) {
      piece p = get_piece_info(B,x,y);
  for (int row = 0; row < BOARD_HEIGHT; row++) {
    int col = x;  //keeping columns constant, iterating over rows
    move_t m1 = (move_t)malloc(sizeof(struct move));
    m1->x1 = x;
    m1->y1 = y;
    m1->x2 = col;
    m1->y2 = row;
    if (check_legal_move(p,B,m1->x2, m1->y2, current_player)) {
      (*possibleMoves).push_back(m1);
    } else {
      free(m1);
    }
  }
  for (int col = 0; col < BOARD_HEIGHT; col++) {
    int row = y;  // keeping rows constant, iterating over columns
    move_t m1 = (move_t)malloc(sizeof(struct move));
    m1->x1 = x;
    m1->y1 = y;
    m1->x2 = col;
    m1->y2 = row;
    if (check_legal_move(p, B,m1->x2, m1->y2,  current_player)) {
      (*possibleMoves).push_back(m1);
    } else {
      free(m1);
    }
  }
}

void gen_moves_pawn(board B, int x, int y,
    std::vector<move_t>* possibleMoves, int current_player) {
      piece p = get_piece_info(B,x,y);
  int mf = (current_player == WHITE) ? 1 : -1;  // white needs to move forward in rows and black needs to move backward, multiply move with this factor
  move_t m1, m2, m3, m4;
  m1 = (move_t)malloc(sizeof(struct move));
  m2 = (move_t)malloc(sizeof(struct move));
  m3 = (move_t)malloc(sizeof(struct move));
  m4 = (move_t)malloc(sizeof(struct move));

  m1->x1 = x;
  m1->y1 = y;
  m2->x1 = x;
  m2->y1 = y;
  m3->x1 = x;
  m3->y1 = y;
  m4->x1 = x;
  m4->y1 = y;

  m1->x2 = x;
  m1->y2 = y + 1 * mf;

  m2->x2 = x;
  m2->y2 = y + 2 * mf;

  m3->x2 = x - 1;
  m3->y2 = y + 1 * mf;

  m4->x2 = x + 1;
  m4->y2 = y + 1 * mf;

  if (check_legal_move(p,B, m1->x2, m1->y2, current_player)) {
    (*possibleMoves).push_back(m1);
  } else {
    free(m1);
  }
  if (check_legal_move(p,B, m2->x2, m2->y2, current_player)) {
    (*possibleMoves).push_back(m2);
  } else {
    free(m2);
  }
  if (check_legal_move(p,B, m3->x2, m3->y2, current_player)) {
    (*possibleMoves).push_back(m3);
  } else {
    free(m3);
  }
  if (check_legal_move(p,B, m4->x2, m4->y2, current_player)) {
    (*possibleMoves).push_back(m4);
  } else {
    free(m4);
  }
}

std::vector<move_t> gen_possible_moves(board B, int current_player) {
  // given current state of board, generate all possible moves
  std::vector<move_t> possibleMoves;

  if (gameOver(B) != -1) {          //check for game over before moving, if true return empty vector
    return possibleMoves;
  }

  for (int row = 0; row < BOARD_HEIGHT; row++) {
    for (int col = 0; col < BOARD_HEIGHT; col++) {
      piece p = get_piece_info(B, col, row);
      
      if (p.type == -1 || (p.p != current_player)) {
        //the current square is empty, or piece is not owned by player
        continue;   //end the iteration
      }

      int x = col;
      int y = row;

      switch (p.type) {
        case KING: {
                     gen_moves_king(B, x, y, &possibleMoves, current_player);
                     break;
                   }
        case QUEEN: {
                      gen_moves_king(B, x, y, &possibleMoves, current_player);
                      break;
                    }
        case BISHOP: {
                       gen_moves_bishop(B, x, y, &possibleMoves, current_player);
                       break;
                     }
        case KNIGHT: {
                       gen_moves_knight(B, x, y, &possibleMoves, current_player);
                       break;
                     }
        case ROOK: {
                     gen_moves_rook(B, x, y, &possibleMoves, current_player);
                     break;
                   }
        case PAWN: {
                     gen_moves_pawn(B, x, y, &possibleMoves, current_player);
                     break;
                   }
      }
    }
  }
  return possibleMoves;

}


// reference: https://chessprogramming.wikispaces.com/Evaluation
int score(board B, int current_player, int mobility, int mobilityOther) { //using the minimax algorithm. white is defined as the max player and is used as reference
  // king
  int wka = (B.player.king.is_active==true) ? 1 : 0;
  int bka = (B.opponent.king.is_active==true) ? 1 : 0;
  float result = 200 * (wka - bka);

  // queen
  int wqa = (B.player.queen.is_active==true) ? 1 : 0;
  int bqa = (B.opponent.queen.is_active==true) ? 1 : 0;
  result += 9 * (wqa - bqa);

  // rooks
  int wra = ((B.player.l_rook.is_active==true) ? 1 : 0) + ((B.player.r_rook.is_active==true) ? 1 : 0);
  int bra = ((B.opponent.l_rook.is_active==true) ? 1 : 0) + ((B.opponent.r_rook.is_active==true) ? 1 : 0);
  result += 5 * (wra - bra);

  // bishops and knights
  int wbka = ((B.player.l_bishop.is_active==true) ? 1 : 0) + ((B.player.r_rook.is_active==true) ? 1 : 0);
  wbka += ((B.player.l_knight.is_active==true) ? 1 : 0) + ((B.player.r_bishop.is_active==true) ? 1 : 0);

  int bbka = ((B.opponent.l_bishop.is_active==true) ? 1 : 0) + ((B.opponent.r_bishop.is_active==true) ? 1 : 0);
  bbka += ((B.opponent.l_knight.is_active==true) ? 1 : 0) + ((B.opponent.r_bishop.is_active==true) ? 1 : 0);
  result += 3 * (wbka - bbka);

  // pawns
  int wpawns = 0;
  int bpawns = 0;
  for (int i = 0; i < BOARD_HEIGHT; i++) {
    if (B.player.pawn[i].is_active == true) wpawns += 1;
    if (B.opponent.pawn[i].is_active == true) bpawns += 1; 
  }
  result += 1 * (wpawns - bpawns);

  result += .1 * (mobility - mobilityOther);

  return result;
}

int flipPlayer(int player) {
  return player == WHITE ? BLACK : WHITE;
}

int max(int a, int b) {
  return a > b ? a : b;
}

int min(int a, int b) {
  return a > b ? b : a;
}

board copy_board(board B) {
  double st, et;
  board board_copy;
  for (int i = 0; i < BOARD_HEIGHT; i++) {
    for (int j = 0; j < BOARD_HEIGHT; j++) {
      piece p =  get_piece_info(B, i,j);
      board_copy.cur_state[i][j] = B.cur_state[i][j];

    }
  }

  board_copy.player = B.player;
  board_copy.opponent = B.opponent;

  return board_copy;
}


minimaxResult_t alphabetaSearch(int curDepth, int maxDepth, int alpha, int beta,
 board board, int curPlayer) {

  double st, et;
  //st = CycleTimer::currentSeconds();
  if (curDepth == maxDepth|| gameOver(board) != -1) {  
    int curScore = score(board, curPlayer, 0, 0);
    minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
    res->bestRes = curScore;    //AI winning or maxdepth reached, so this is the best result
    res->move = NULL;
    return res;
  }

  std::vector<move_t> possibleMoves = gen_possible_moves(board, curPlayer);
  if (curDepth == 0) {
    std::random_shuffle(possibleMoves.begin(), possibleMoves.end());
  }

  move_t bestMove = NULL;


  for (std::vector<move_t>::iterator it = possibleMoves.begin(); it != possibleMoves.end(); it++) {   //evaluate feasibility of each posible move

    move_t curMove = *it;
    int x1 = curMove->x1;
    int y1 = curMove->y1;
    piece startPiece = get_piece_info(board, x1,y1);
    int x2 = curMove->x2;
    int y2 = curMove->y2;
    piece endPiece = get_piece_info(board, x1,y1);

  
    make_move(curMove, board, curPlayer); 
    

    minimaxResult_t curRes = alphabetaSearch(curDepth + 1, maxDepth, -beta, -alpha, board,
      flipPlayer(curPlayer));   //increase depth and evaluate moves of player
    int resS = -(curRes->bestRes);

    // now undo this move
    undoMove(x1, y1, startPiece, x2, y2, endPiece, board, curPlayer); //dont want to actually make a move, just wanted to see possible moves user can make

    if (beta <= resS) { 
      free(curRes);
      minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
      res->bestRes = beta;  
      res->move = bestMove;
      return res;
    }
    if (alpha < resS) {
      bestMove = curMove;
      alpha = resS;
    }
    free(curRes);
  }
  minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
  res->bestRes = alpha;
  res->move = bestMove;
  return res;
}


minimaxResult_t PVSplit(int curDepth, int maxDepth, int alpha, int beta,
  board B, int curPlayer) {

  if (maxDepth == curDepth || gameOver(B) != -1) {
    int curScore = score(B, curPlayer, 0, 0);
    minimaxResult_t result = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
    result->bestRes = curScore;
    result->move = NULL;
    return result;
  }

  std::vector<move_t> possibleMoves = gen_possible_moves(B, curPlayer);

  if (curDepth == 0) {
    std::random_shuffle(possibleMoves.begin(), possibleMoves.end());
  }

  move_t bestMove = NULL;

  move_t firstMove = possibleMoves.at(0);
  int x1 = firstMove->x1;
  int y1 = firstMove->y1;
  piece startPiece = get_piece_info(B, x1,y1);

  int x2 = firstMove->x2;
  int y2 = firstMove->y2;
  piece endPiece = get_piece_info(B, x1,y1);

  check_legal_move(endPiece, B, firstMove->x2, firstMove->y2, curPlayer);
  make_move(firstMove, B, curPlayer);

  minimaxResult_t firstRes = PVSplit(curDepth + 1, maxDepth, -beta, -alpha, B, flipPlayer(curPlayer));
  firstRes->bestRes =   -(firstRes->bestRes);
  int result_opponent = firstRes->bestRes;

  undoMove(x1, y1, startPiece, x2, y2, endPiece, B, curPlayer);

  if (result_opponent > beta) {
    free(firstRes);
    minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
    res->bestRes = beta;
    res->move = bestMove;
    return res;
  }
  if (alpha < result_opponent) {
    bestMove = firstMove;
    alpha = result_opponent;
  }

  pthread_t threads[int(possibleMoves.size()) - 1];

  int threadStatus;
  if (int(possibleMoves.size()) > 1)  {
    volatile bool flag = false;
    int i;
    #pragma omp parallel for default(shared) shared(flag, alpha, bestMove) private(i) schedule(dynamic)
    for (i = 1; i < possibleMoves.size(); i++) 
    {

      if (flag) {
        continue;
      }
      else 
      {
        move_t curMove = possibleMoves.at(i);
        int x1 = curMove->x1;
        int y1 = curMove->y1;
        piece startPiece = get_piece_info(B, x1,y1);
        int x2 = curMove->x2;
        int y2 = curMove->y2;
        piece endPiece = get_piece_info(B, x2,y2);  
        board board_copy;     //create a copy to evaluate the move
        board_copy = copy_board(B);
        check_legal_move(endPiece, B, curMove->x2, curMove->y2, curPlayer);
        make_move(curMove, board_copy, curPlayer);
        minimaxResult_t curRes = alphabetaSearch(curDepth + 1, maxDepth, -beta, -alpha, board_copy, flipPlayer(curPlayer));
        curRes->bestRes = -(curRes->bestRes);
        int iresS = curRes->bestRes;

        if (iresS > beta) {
          free(curRes);
          minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
          res->bestRes = beta;
          res->move = bestMove;
          flag = true;
        }
        //only want one thread here
        #pragma omp critical
          if (alpha < iresS) {
            bestMove = curMove;
            alpha = iresS;
          }
        }
    }
  }
  minimaxResult_t res = (minimaxResult_t)malloc(sizeof(struct minimaxResult));
  res->bestRes = alpha;
  res->move = bestMove;
  return res;
}

move_t nextMove(board board, int aiType, int curPlayer) 
{
  std::srand ( unsigned ( std::time(0) ) );
  minimaxResult_t res;
  res = PVSplit(0, MAXDEPTH, NEGINF, POSINF, board, curPlayer);
  move_t move = res->move;
  free(res);
  return move;
}
