#include "board.h"
#include <iostream>
#include "stdio.h"

using namespace std;

void initialize_board(board &B)         //intialize all the pieces
{
    for(int j = 0 ; j < 8 ; j++)
        for(int i = 0 ; i < 8 ; i++)
            B.cur_state[i][j] = -1;     //-1 means empty square         

    B.player.king.type = KING;          //assign each piece one by one
    update_pos(B.player.king, B, 4, 0);
    B.player.king.is_active = true;
    B.player.king.p = WHITE;

    B.player.queen.type = QUEEN;
    update_pos(B.player.queen, B, 3, 0);
    B.player.queen.is_active = true;
    B.player.queen.p = WHITE;

    B.player.l_bishop.type = BISHOP;
    update_pos(B.player.l_bishop, B, 2, 0);
    B.player.l_bishop.is_active = true;
    B.player.l_bishop.p = WHITE;

    B.player.r_bishop.type = BISHOP;
    update_pos(B.player.r_bishop, B, 5, 0);
    B.player.r_bishop.is_active = true;
    B.player.r_bishop.p = WHITE;

    B.player.l_knight.type = KNIGHT;
    update_pos(B.player.l_knight, B, 1, 0);
    B.player.l_knight.is_active = true;
    B.player.l_knight.p = WHITE;

    B.player.r_knight.type = KNIGHT;
    update_pos(B.player.r_knight, B, 6, 0);
    B.player.r_knight.is_active = true;
    B.player.r_knight.p = WHITE;

    B.player.r_rook.type = ROOK;
    update_pos(B.player.r_rook, B, 7, 0);
    B.player.r_rook.is_active = true;
    B.player.r_rook.p = WHITE;

    for(int i = 0 ; i < 8 ; i++)
    {
        B.player.pawn[i].type = PAWN;
        B.player.pawn[i].p = WHITE;
        update_pos(B.player.pawn[i], B, i, 1);
        B.player.pawn[i].is_active = true;
        
    }
    
    B.opponent.king.type = KING;
    B.opponent.king.p = BLACK;
    update_pos(B.opponent.king, B, 4, 7);
    B.opponent.king.is_active = true;

    B.opponent.queen.type = QUEEN;
    B.opponent.queen.p = BLACK;
    update_pos(B.opponent.queen, B, 3, 7);
    B.opponent.queen.is_active = true;

    B.opponent.l_bishop.type = BISHOP;
    B.opponent.l_bishop.p = BLACK;
    update_pos(B.opponent.l_bishop, B, 2, 7);
    B.opponent.l_bishop.is_active = true;

    B.opponent.r_bishop.type = BISHOP;
    B.opponent.r_bishop.p = BLACK;
    update_pos(B.opponent.r_bishop, B, 5, 7);
    B.opponent.r_bishop.is_active = true;

    B.opponent.l_knight.type = KNIGHT;
    B.opponent.l_knight.p = BLACK;
    update_pos(B.opponent.l_knight, B, 1, 7);
    B.opponent.l_knight.is_active = true;   

    B.opponent.r_knight.type = KNIGHT;
    B.opponent.r_knight.p = BLACK;
    update_pos(B.opponent.r_knight, B, 6, 7);
    B.opponent.r_knight.is_active = true;

    B.opponent.l_rook.type = ROOK;
    B.opponent.l_rook.p = BLACK;
    update_pos(B.opponent.l_rook, B, 0, 7);
    B.opponent.l_rook.is_active = true;

    B.opponent.r_rook.type = ROOK;
    B.opponent.r_rook.p = BLACK;
    update_pos(B.opponent.r_rook, B, 7, 7);
    B.opponent.r_rook.is_active = true;
    

    for(int i = 0 ; i < 8 ; i++)
    {
        B.opponent.pawn[i].type = PAWN;
        B.opponent.pawn[i].p = BLACK;
        update_pos(B.opponent.pawn[i], B, i, 6);
        B.opponent.pawn[i].is_active = true;
    }

    B.player.l_rook.type = ROOK;
    update_pos(B.player.l_rook, B, 0, 0);
    B.player.l_rook.is_active = true;
    B.player.l_rook.p = WHITE;
}

void update_pos(piece &p, board &B, int val_x, int val_y)       //go through each piece and match using the current position. then update the position of the actual piece
{
    if(p.p == WHITE)
    {
        if(p.type == 1)
        {
            for(int i = 0 ; i < 8; i++)
            {
                if(B.player.pawn[i].cur_pos_x == p.cur_pos_x && B.player.pawn[i].cur_pos_y == p.cur_pos_y)
                {
                    B.player.pawn[i].cur_pos_x = val_x;
                    B.player.pawn[i].cur_pos_y = val_y;
                }
            }
        }
        else if(p.type == 2)
        {
            if(B.player.r_rook.cur_pos_x == p.cur_pos_x && B.player.r_rook.cur_pos_y == p.cur_pos_y)
            {
                B.player.r_rook.cur_pos_x = val_x;
                B.player.r_rook.cur_pos_y = val_y;
            }
            else if(B.player.l_rook.cur_pos_x == p.cur_pos_x && B.player.l_rook.cur_pos_y == p.cur_pos_y)
            {
                B.player.l_rook.cur_pos_x = val_x;
                B.player.l_rook.cur_pos_y = val_y;
            }
            
        }
        else if(p.type == 3)
        {
            if(B.player.l_knight.cur_pos_x == p.cur_pos_x && B.player.l_knight.cur_pos_y == p.cur_pos_y)
            {
                B.player.l_knight.cur_pos_x = val_x;
                B.player.l_knight.cur_pos_y = val_y;
            }
            else if(B.player.r_knight.cur_pos_x == p.cur_pos_x && B.player.r_knight.cur_pos_y == p.cur_pos_y)
            {
                B.player.r_knight.cur_pos_x = val_x;
                B.player.r_knight.cur_pos_y = val_y;
            }
        }
        else if(p.type == 4)
        {
            if(B.player.l_bishop.cur_pos_x == p.cur_pos_x && B.player.l_bishop.cur_pos_y == p.cur_pos_y)
            {
                B.player.l_bishop.cur_pos_x = val_x;
                B.player.l_bishop.cur_pos_y = val_y;
            }
            else if(B.player.r_bishop.cur_pos_x == p.cur_pos_x && B.player.r_bishop.cur_pos_y == p.cur_pos_y)
            {
                B.player.r_bishop.cur_pos_x = val_x;
                B.player.r_bishop.cur_pos_y = val_y;
            }
        }
        else if(p.type == 5)
        {
            if(B.player.queen.cur_pos_x == p.cur_pos_x && B.player.queen.cur_pos_y == p.cur_pos_y)
            {
                B.player.l_bishop.cur_pos_x = val_x;
                B.player.l_bishop.cur_pos_y = val_y;
            }
        }
        else if(p.type == 6)
        {
            if(B.player.king.cur_pos_x == p.cur_pos_x && B.player.king.cur_pos_y == p.cur_pos_y)
            {
                B.player.king.cur_pos_x = val_x;
                B.player.king.cur_pos_y = val_y;
            }
        }
    }
    else if(p.p == BLACK)
    {
        if(p.type == 1)
        {
            for(int i = 0 ; i < 8; i++)
            {
                if(B.opponent.pawn[i].cur_pos_x == p.cur_pos_x && B.opponent.pawn[i].cur_pos_y == p.cur_pos_y)
                {
                    B.opponent.pawn[i].cur_pos_x = val_x;
                    B.opponent.pawn[i].cur_pos_y = val_y;
                }
            }
        }
        else if(p.type == 2)
        {
            if(B.opponent.l_rook.cur_pos_x == p.cur_pos_x && B.opponent.l_rook.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.l_rook.cur_pos_x = val_x;
                B.opponent.l_rook.cur_pos_y = val_y;
            }
            else if(B.opponent.r_rook.cur_pos_x == p.cur_pos_x && B.opponent.r_rook.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.r_rook.cur_pos_x = val_x;
                B.opponent.r_rook.cur_pos_y = val_y;
            }
        }
        else if(p.type == 3)
        {
            if(B.opponent.l_knight.cur_pos_x == p.cur_pos_x && B.opponent.l_knight.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.l_knight.cur_pos_x = val_x;
                B.opponent.l_knight.cur_pos_y = val_y;
            }
            else if(B.opponent.r_knight.cur_pos_x == p.cur_pos_x && B.opponent.r_knight.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.r_knight.cur_pos_x = val_x;
                B.opponent.r_knight.cur_pos_y = val_y;
            }
        }
        else if(p.type == 4)
        {
            if(B.opponent.l_bishop.cur_pos_x == p.cur_pos_x && B.opponent.l_bishop.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.l_bishop.cur_pos_x = val_x;
                B.opponent.l_bishop.cur_pos_y = val_y;
            }
            else if(B.opponent.r_bishop.cur_pos_x == p.cur_pos_x && B.opponent.r_bishop.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.r_bishop.cur_pos_x = val_x;
                B.opponent.r_bishop.cur_pos_y = val_y;
            }
        }
        else if(p.type == 5)
        {
            if(B.opponent.queen.cur_pos_x == p.cur_pos_x && B.opponent.queen.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.l_bishop.cur_pos_x = val_x;
                B.opponent.l_bishop.cur_pos_y = val_y;
            }
        }
        else if(p.type == 6)
        {
            if(B.opponent.king.cur_pos_x == p.cur_pos_x && B.opponent.king.cur_pos_y == p.cur_pos_y)
            {
                B.opponent.king.cur_pos_x = val_x;
                B.opponent.king.cur_pos_y = val_y;
            }
        }
    }

    B.cur_state[p.cur_pos_x][p.cur_pos_y] = -1;
    p.cur_pos_x = val_x;
    p.cur_pos_y = val_y;
    B.cur_state[p.cur_pos_x][p.cur_pos_y] = p.type;
}

void print_board(board B)       //function to print the board
{

    for(int j = 7 ; j >= 0 ; j--)
    {
        for(int i = 7 ; i >= 0 ; i--)
        {
                cout << B.cur_state[i][j] << "\t";
        }
        cout << "\n";
    }

    cout << "\n";
}

