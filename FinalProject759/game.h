#ifndef GAME_H
#define GAME_H

#define POSINF INT_MAX-1     // may need to include <limits.h>
#define NEGINF INT_MIN+1

#define MAXDEPTH 8

#define MAXI 1
#define MINI 2

typedef struct minimaxResult* minimaxResult_t;
typedef struct movesListElem* movesListElem_t;

struct minimaxResult {
  int bestRes;
  move_t move;
};

move_t nextMove(board board, int aiType, int curPlayer);
bool check_legal_move(piece p, board B, int x, int y ,int current_player);
int gameOver(board B);
void make_move(move_t move, board &B, int curPlayer);
void undoMove(int x1, int y1, piece startPiece, int x2, int y2,piece endPiece, board B, int curPlayer);
board copy_board(board B);

#endif
