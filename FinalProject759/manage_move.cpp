#include "board.h"
#include "manage_move.h"
#include <iostream>
#include "stdio.h"
#include "game.h"
#include "manage_move.h"
#include <cstring>
#include <omp.h>

#include<ctime>


#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;
using std::string;
int AI = 0;

piece get_piece_info(board B, int x, int y)     //given a current x and y, this function returns the piece information of that square
{
    int piece_type = B.cur_state[x][y];
    piece p;
    if(piece_type == -1)
    {
        p.type = -1 ;
        return p;
    } 
    else if(piece_type == 1)
    {
        for(int i = 0 ; i < 8 ; i++)
        {
                if(B.player.pawn[i].cur_pos_x == x && B.player.pawn[i].cur_pos_y == y)
                    return B.player.pawn[i];
                else if((B.opponent.pawn[i].cur_pos_x == x && B.opponent.pawn[i].cur_pos_y == y))
                    return B.opponent.pawn[i];
        }
    }
    else if(piece_type == 2)
    {
        if(B.player.l_rook.cur_pos_x == x && B.player.l_rook.cur_pos_y == y)
                return B.player.l_rook;
        else if((B.player.r_rook.cur_pos_x == x && B.player.r_rook.cur_pos_y == y))
                return B.player.r_rook;
        else if(B.opponent.l_rook.cur_pos_x == x && B.opponent.l_rook.cur_pos_y == y)
                return B.opponent.l_rook;
        else if((B.opponent.r_rook.cur_pos_x == x && B.opponent.r_rook.cur_pos_y == y))
                return B.opponent.r_rook;
    }
    else if(piece_type == 3)
    {
        if(B.player.l_knight.cur_pos_x == x && B.player.l_knight.cur_pos_y == y)
                return B.player.l_knight;
        else if((B.player.r_knight.cur_pos_x == x && B.player.r_knight.cur_pos_y == y))
                return B.player.r_knight;
        else if(B.opponent.l_knight.cur_pos_x == x && B.opponent.l_knight.cur_pos_y == y)
                return B.opponent.l_knight;
        else if((B.opponent.r_knight.cur_pos_x == x && B.opponent.r_knight.cur_pos_y == y))
                return B.opponent.r_knight;
    }
    else if(piece_type == 4)
    {
        if(B.player.l_bishop.cur_pos_x == x && B.player.l_bishop.cur_pos_y == y)
                return B.player.l_bishop;
        else if((B.player.r_bishop.cur_pos_x == x && B.player.r_bishop.cur_pos_y == y))
                return B.player.r_bishop;
        else if(B.opponent.l_bishop.cur_pos_x == x && B.opponent.l_bishop.cur_pos_y == y)
                return B.opponent.l_bishop;
        else if((B.opponent.r_bishop.cur_pos_x == x && B.opponent.r_bishop.cur_pos_y == y))
                return B.opponent.r_bishop;
    }
    else if(piece_type == 5)
    {
        if(B.player.queen.cur_pos_x == x && B.player.queen.cur_pos_y == y)
                return B.player.queen;
        else if((B.opponent.queen.cur_pos_x == x && B.opponent.queen.cur_pos_y == y))
                return B.opponent.queen;      
    }
    else if(piece_type == 6)
    {
        if(B.player.king.cur_pos_x == x && B.player.king.cur_pos_y == y)
                return B.player.king;
        else if((B.opponent.king.cur_pos_x == x && B.opponent.king.cur_pos_y == y))
                return B.opponent.king;      
    }
    piece not_found; 
    not_found.type = -1;
    not_found.is_active = false;
  return not_found;
}

bool isValidMoveFormat(char move[]) {
  //printf(move);
  if (std::strlen(move) != 6) {
    printf("length = %d\n", std::strlen(move));
    return false;
  }

  else if (move[0] != 'a' && move[0] != 'b' && move[0] != 'c' && move[0] != 'd' &&
      move[0] != 'e' && move[0] != 'f' && move[0] != 'g' && move[0] != 'h') {
    return false;
  }

  else if (move[1] != '0' && move[1] != '1' && move[1] != '2' && move[1] != '3' &&
      move[1] != '4' && move[1] != '5' && move[1] != '6' && move[1] != '7') {
    return false;
  }

  else if (move[2] != 't' && move[3] != 'o') {
    return false;
  }

  else if (move[4] != 'a' && move[4] != 'b' && move[4] != 'c' && move[4] != 'd' &&
      move[4] != 'e' && move[4] != 'f' && move[4] != 'g' && move[4] != 'h') {
    return false;
  }

  else if (move[5] != '0' && move[5] != '1' && move[5] != '2' && move[5] != '3' &&
      move[5] != '4' && move[5] != '5' && move[5] != '6' && move[5] != '7') {
    return false;
  }
  
  
  else
  {
  return true;
  } 
}
void remove_piece(piece &p , board &B)
{
    p.is_active = false;
    B.cur_state[p.cur_pos_x][p.cur_pos_y] = -1;
}

void make_move(move_t move, board &B, int curPlayer) {
  
  int x1 = move->x1;
  int x2 = move->x2;
  int y1 = move->y1;
  int y2 = move->y2;

  piece startIndex = get_piece_info(B, move->x1, move->y1);
  piece endIndex = get_piece_info(B, move->x2,move->y2 );

  if (endIndex.type != -1 && endIndex.p != curPlayer) {
    if (curPlayer == WHITE) {
      remove_piece(endIndex, B);
      update_pos(startIndex, B, move->x2, move->y2);
      piece p = get_piece_info(B, move->x2, move->y2);
    }
    else {
      remove_piece(endIndex, B);
      update_pos(startIndex, B, move->x2, move->y2);
      piece p = get_piece_info(B, move->x2, move->y2);
    }
  }
  else {
    update_pos(startIndex, B, move->x2, move->y2);
    piece p = get_piece_info(B, move->x2, move->y2);    
  }
}

bool check_legal_move(piece p, board B, int x, int y, int current_player)
{
    int cur_x = p.cur_pos_x;
    int cur_y = p.cur_pos_y;

    int steps_x = cur_y-y;
    int steps_y = cur_x-x;
    int print = 0;
    
    if(x < 0 || x >7 || y <0 || y>7)
    {
        if(p.p == WHITE && print== 1)
            cout << "Out of bounds move\n";
        return false; 
    }

    //is there a piece at this position
    piece target_pos = get_piece_info(B, p.cur_pos_x, p.cur_pos_y);
    if(target_pos.type == -1)
    {
        if(p.p == WHITE && print== 1)            //only print if the user is moving
            cout << "No piece at given position\n";        
        return false; 
    }
    else if(p.p != current_player)
    {
        if(p.p == WHITE && print== 1)            //only print if the user is moving
            cout << "Piece is not yours\n";
        return false; 
    }
    else if(target_pos.p == current_player && target_pos.cur_pos_x == x && target_pos.cur_pos_y == y)   //if the piece at the target position is your own piece, condition 1 = piece at target position matches type of p, 
    {
        if(p.p == WHITE && print== 1)            //only print if the user is moving
            cout << "Piece at target position is yours\n";
        return false; 
    }   

    target_pos = get_piece_info(B, x, y);
    
    //check legal move based on the type of piece
    if(p.type == PAWN && current_player == WHITE)
    {
        if(cur_y != 1 && steps_x <= -2)
        {
            if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawn can only move one place if not at starting position\n";
            return false;
        }
        else if(cur_y == 1 && steps_x < -2)     // if pawn is in starting position, it can move up to 2 moves
        {
            if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawns can only move 2 spaces on first move.\n";
            return false;
        }
        else if(steps_x >= 0)// pawns can only move forward, not sideways or backward
        {
            if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawns can only move forward.\n";
            return false;
        }
        else if(abs(steps_y) > 1)   //pawns can move one position horizaontally only by one position
        {
            if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawns can't move that much horizontally.\n";
            return false;
        }
        else if (abs(steps_y) == 1 and steps_x == -1 && target_pos.type == -1) 
        {
            if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawns can only move diagonally if there is an enemy piece there.\n";
            return false;
        }
        // pawns can't move directly forward into an enemy piece
        else if (steps_x == -1 && steps_y == 0 && target_pos.type != -1) 
        {
           if(p.p == WHITE && print== 1)            //only print if the user is moving
                cout << "Pawns can't move directly forward into an enemy piece.\n";
            return false;
        }
        else
            return true;

    }
    else if(p.type == PAWN && current_player == BLACK)
    {
        if(p.cur_pos_y != 6 && steps_x >= 2)
        {
            //if(p.p == BLACK)            //only print if the user is moving
            //cout << "Pawn can only move one place if not at starting position\n";
            return false;
        }
        else if(p.cur_pos_y == 6 && steps_x > 2)     // if pawn is in starting position, it can move up to 2 moves
        {
            //if(p.p == BLACK)            //only print if the user is moving
              //  cout << "Pawns can only move 2 spaces on first move.\n";
            return false;
        }
        else if(steps_x <= 0)// pawns can only move forward, not sideways or backward
        {
            //if(p.p == BLACK)            //only print if the user is moving
              //  cout << "Pawns can only move forward.\n";
            return false;
        }
        else if(abs(steps_y) > 1)   //pawns can move one position horizaontally only by one position
        {
            //if(p.p == BLACK)            //only print if the user is moving
              //  cout << "Pawns can't move that much horizontally.\n";
            return false;
        }
        else if (abs(steps_y) == 1 and steps_x == 1 && target_pos.type == -1) 
        {
            //if(p.p == BLACK)            //only print if the user is moving
              //  cout << "Pawns can only move diagonally if there is an enemy piece there.\n";
            return false;
        }
        // pawns can't move directly forward into an enemy piece
        else if (steps_x == 1 && steps_y == 0 && target_pos.type == -1) 
        {
           //if(p.p == BLACK)            //only print if the user is moving
             //   cout << "Pawns can't move directly forward into an enemy piece.\n";
            return false;
        }
        else
            return true;
    }
    else if (p.type == KING) 
    {
        // kings can only move one space
        if (abs(steps_x) > 1 || abs(steps_y) > 1) 
        {
            if (p.p == WHITE && print== 1 && print== 1)
                cout << "Kings an only move one space in any direction.\n";
            return false;
        }
    else return true;
    }

    else if (p.type == QUEEN) 
    {
        if (abs(steps_x) != abs(steps_y) && steps_x != 0 && steps_y != 0) 
        {
            if (p.p == WHITE && print== 1) 
                cout << "Queens can only move diagonally or straight.\n";
            return false;
    }

    if (steps_x > 0 && steps_y > 0) {
      int j = cur_x;
      for (int i = cur_y-1; i > y; i--) {
        j--;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y < 0) {
      int j = cur_x;
      for (int i = cur_y+1; i < y; i++) {
        j++;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x > 0 && steps_y < 0) {
      int j = cur_x;
      for (int i = cur_y-1; i > y; i--) {
        j++;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y > 0) {
      int j = cur_x;
      for (int i = cur_y+1; i < y; i++) {
        j--;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x == 0 && steps_y > 0) {
      for (int i = cur_x-1; i > x; i--) {
        piece tmp = get_piece_info(B, i, cur_y);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x == 0 && steps_y < 0) {
      for (int i = cur_x+1; i < x; i++) {
        piece tmp = get_piece_info(B, i, cur_y);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x > 0 && steps_y == 0) {
      for (int i = cur_y-1; i > y; i--) {
        piece tmp = get_piece_info(B , cur_x, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y == 0) {
      for (int i = cur_y+1; i < y; i++) {
        piece tmp = get_piece_info(B, cur_x, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else return true;
  }

   else if (p.type == KNIGHT) {
    if (abs(steps_x) * abs(steps_y) != 2) {
      if (p.p == WHITE && print== 1)  
        cout << "Illegal move for a knight.\n";
      return false;
    }

    else return true;

  }

  else if (p.type == BISHOP) {
    if (abs(steps_x) != abs(steps_y)) {
      if (p.p == WHITE && print== 1) 
        cout << "Bishops can only move diagonally.\n";
      return false;
    }
    // check all the blocking pieces
    else if (steps_x > 0 && steps_y > 0) {
      int j = cur_x;
      for (int i = cur_y-1; i > y; i--) {
        j--;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
            if (p.p == WHITE && print== 1) 
                cout << "There is a piece blocking your move.\n";
            return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y < 0) {
      int j = cur_x;
      for (int i = cur_y+1; i < y; i++) {
        j++;
        piece tmp = get_piece_info(B, j, i);
        if (tmp.type != -1) {
            if (p.p == WHITE && print== 1) 
                cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x > 0 && steps_y < 0) {
      int j = cur_x;
      for (int i = cur_y-1; i > y; i--) {
        j++;
        piece tmp = get_piece_info(B,j, i);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1)  
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y > 0) {
      int j = cur_x;
      for (int i = cur_y+1; i < y; i++) {
        j--;
        piece tmp = get_piece_info(B,j, i);
        if (tmp.type != -1) {
            if (p.p == WHITE && print== 1)  
                cout << "There is a piece blocking your move.\n";
            return false;
        }
      }
    }
    else return true;
  }
  else if (p.type == ROOK) {
    if (steps_x != 0 && steps_y != 0) {
      if (p.p == WHITE && print== 1) 
        cout << "Rooks can only move directly forward or sideways.\n";
      return false;
    }
    // check for blocking pieces
    else if (steps_x == 0 && steps_y > 0) {
      for (int i = cur_x-1; i > x; i--) {
        piece tmp = get_piece_info(B,i, cur_y);
        if (tmp.type != -1) {
            if (p.p == WHITE && print== 1) 
                cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x == 0 && steps_y < 0) {
      for (int i = cur_x+1; i < x; i++) {
        piece tmp = get_piece_info(B,i, cur_y);
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x > 0 && steps_y == 0) {
      for (int i = cur_y-1; i > y; i--) {
        piece tmp = get_piece_info(B,cur_x, i);
        
        if (tmp.type != -1) {
          if (p.p == WHITE && print== 1) 
            cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }
    else if (steps_x < 0 && steps_y == 0) {
      for (int i = cur_y+1; i < y; i++) {
        piece tmp = get_piece_info(B,cur_x, i);
        if (tmp.type != -1) {
            if (p.p == WHITE && print== 1) 
                cout << "There is a piece blocking your move.\n";
          return false;
        }
      }
    }

    else return true;
  }

  return false;
}



void undoMove(int x1, int y1, piece startPiece, int x2, int y2,
 piece endPiece, board B, int curPlayer) {    //SK:should pass board and piece as call by reference?
  piece start_piece = get_piece_info(B, x1, y1);//getIndex(x1, y1); 
  piece end_piece = get_piece_info(B, x2, y2);//getIndex(x2, y2);

  //board_piece_t gameBoard = B->gameBoard;

  //gameBoard[startIndex] = startPiece;
  //gameBoard[endIndex] = endPiece;

  // int curPlayer = B->curPlayer;
  piece p;
  if (curPlayer == WHITE) {
    p.p = BLACK;
  }
  else if (curPlayer == BLACK) {
    p.p = WHITE;
  }

  // logic for bringing a piece back from the dead if it was taken
  if (end_piece.type == -1) {
    if(end_piece.is_active == false && end_piece.p != curPlayer)
    {
      end_piece.is_active == true;
      B.cur_state[end_piece.cur_pos_x][end_piece.cur_pos_y] == end_piece.type;
    }
  }
}


move_t parseMove (string move) {
  int x1, y1, x2, y2;

  
  y1 = (int)move[1] - '0';
  y2 = (int)move[5] - '0';

  switch (move[0]) {
    case 'a': x1 = 0;
              break;
    case 'b': x1 = 1;
              break;
    case 'c': x1 = 2;
              break;
    case 'd': x1 = 3;
              break;
    case 'e': x1 = 4;
              break;
    case 'f': x1 = 5;
              break;
    case 'g': x1 = 6;
              break;
    case 'h': x1 = 7;
              break;
  }

  switch (move[4]) {
    case 'a': x2 = 0;
              break;
    case 'b': x2 = 1;
              break;
    case 'c': x2 = 2;
              break;
    case 'd': x2 = 3;
              break;
    case 'e': x2 = 4;
              break;
    case 'f': x2 = 5;
              break;
    case 'g': x2 = 6;
              break;
    case 'h': x2 = 7;
              break;
  }

  move_t m = (move_t)malloc(sizeof(move));
  m->x1 = x1;
  m->y1 = y1;
  m->x2 = x2;
  m->y2 = y2;

  return m;
}

int gameOver(board B) {
  if (!B.player.king.is_active) {
    return BLACK;
  }
  else if (!B.opponent.king.is_active) {
    return WHITE;
  }
  else {
    return -1;
  }
}

int main() {
  board B ;
  
  initialize_board(B);
  print_board(B);
  char move[10];
  int numMoves = 0;
  omp_set_num_threads(8);

  int i =1;
  while(1) {
    numMoves++;
  move_t m;

  if (i==1) {
      i=2;
      AI = 0;
      move[0] = '\0';
      std::cout << "Please enter a move: \n";
      std::cin >> move;

      if(move[0] == 'p')
        print_board(B);
      while(!isValidMoveFormat(move)) {
        move[0] = '\0';
        std::cout << "Please enter a move(invalid):\n ";
        // get move
        std::cin >> move;
      }

      if(move[0] == 'p')
        print_board(B);
      m = parseMove(move);

      piece p = get_piece_info(B, m->x1, m->y1);

      while (!check_legal_move(p, B, m->x2, m->y2, WHITE)) 
      { 
        move[0] = '\0';
        std::cout << "Please enter a move(illegal):\n ";
        std::cin >> move;
        if(move[0] == 'p')
          print_board(B);
        while(!isValidMoveFormat(move)) {
          // get move
          move[0] = '\0';
          std::cout << "Please enter a move(invalid):\n ";
          std::cin >> move;
        }
       
        m = parseMove(move);
        
        piece p = get_piece_info(B, m->x1, m->y1);
                
      }
    } else if(i==2) {
      i=1;
     
      AI = 1;
      double min = 20000000.f;
      double startTime, endTime;
      high_resolution_clock::time_point start, end;
      duration<double , std::milli> duration_sec;

      start = high_resolution_clock::now();

      m = nextMove(B, MAXI, BLACK);
      end = high_resolution_clock::now();
      duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);

      printf("AI (W) decided move (%d, %d) (%d,%d)) in time",m->x1, m->y1, m->x2, m->y2);
      cout << duration_sec.count() << std::endl;
    }

    int curPlayer = (i==2) ? WHITE : (i==1) ? BLACK : -1;
    make_move(m , B, curPlayer);
    piece p = get_piece_info(B, m->x2, m->y2);
    print_board(B);
    
    // check game over
    if (gameOver(B) == WHITE) {
      printf("White wins!\n");
      break;
    }

    else if (gameOver(B) == BLACK) {
      printf("Black wins!\n");
      break;
    }
    if(numMoves > 100) {return 0;}

  }
  return 0;
}
