#ifndef BOARD_h
#define BOARD_H

#define BOARD_HEIGHT 8 

enum piece_type
{
    PAWN = 1, 
    ROOK = 2,
    KNIGHT = 3,
    BISHOP = 4,
    QUEEN = 5,
    KING = 6
};

enum player
{
    WHITE = 0,
    BLACK = 1
};

struct piece
{
    int type;
    bool is_active;
    player p;
    int cur_pos_x;
    int cur_pos_y; 
};

struct one_side{
    piece king, queen, l_bishop, r_bishop, l_knight, r_knight, l_rook, r_rook;
    piece pawn[8];
};

struct board{
    one_side player;
    one_side opponent; 
    int cur_state[BOARD_HEIGHT][BOARD_HEIGHT];
};
typedef struct move* move_t;
struct move {
  // x cols, y rows
  int x1;
  int y1;
  int x2;
  int y2;
};

void initialize_board(board &B);
void update_pos(piece &p, board &B, int val_x, int val_y);
void print_board(board B);
piece get_piece_info(board B, int x, int y);


#endif


