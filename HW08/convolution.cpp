#include<iostream>
#include"convolution.h"


using namespace std;

void convolve(const float *image, float *output, std::size_t n, const float *mask, std::size_t m)
{
  float acc;
  std::size_t x, y, i, j;
  //i j k local to each thread and image and mask shared between all threads
  #pragma omp parallel for private(x,y,i,j) shared(image,mask)
	for( x = 0 ; x < n ; x++)
	{
			for( y = 0 ; y< n ; y++)
			{
        acc=0;
				float f;
				for( i = 0 ; i < m ; i ++)
				{
				for( j = 0 ;  j < m ; j++)
				{
					//cout << "i = " << i << "\t j = " << j << endl;
					//cout << "mask" << mask[i*m + j] << endl;

					int x_index = x + i - ((m-1)/2);
					int y_index = y + j - ((m-1)/2);

					//cout << "x_index = " << x_index << "\t y_index = " << y_index << endl;
					if(x_index < n && x_index >=0 )      //padding algorithm from old HW
					{
						if(y_index < n && y_index >=0)
						{
							f = image[n*x_index + y_index];
						}
						else
							f = 1;
					}
					else
					{
						if(y_index < n && y_index >=0)
						{
							f = 1;
						}
						else
							f = 0;
					}
					//cout << "f = " << f << endl;
					output[n*x + y] += mask[i*m + j] * f;    //assign to final output
					//cout << "output" << output[0] << endl;
				}
				}
				//cout << output[n*x+y] << "\t";
			}
			//cout << endl;
	}
	//cout << "final output" << output[0] << endl;
}
