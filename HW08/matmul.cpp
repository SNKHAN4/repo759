#include<iostream>
#include"matmul.h"


using namespace std;
void mmul(const float* A, const float* B, float* C, const std::size_t n)
{
  std::size_t i, j, k;
  //private i j k for all threads and ABC are shared
  #pragma omp parallel for private(i,j,k) shared(A,B,C)
    for( i = 0 ; i < n ; i++)
    {
      //printf("I am thread number %d\n", omp_get_thread_num());
      for( k = 0 ; k < n ; k++)
      {
        for( j = 0 ; j < n ; j++)
        {
          C[n*i + j] += A[n*i +k] * B[n*k + j];
        }
      }
    }

}
