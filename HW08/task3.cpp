#include<iostream>
#include<ctime>
#include <chrono>
#include <ratio>
#include <cmath>

#include"msort.h"

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

void printArr(int *array, int size) {
   for(int i = 0; i<size; i++)
      std::cout << array[i] << "\t";
   std::cout << endl;
}

int main(int argc , char *argv[])
{
  const std::size_t n = std::stoi(argv[1]);     //command line arguments
  const std::size_t t = std::stoi(argv[2]);
  const std::size_t ts = std::stoi(argv[3]);

  high_resolution_clock::time_point start, end;
  duration<double , std::milli> duration_sec;

  srand(time(0));

  int* arr = new int[n];
  for(std::size_t i = 0 ; i < n ; i ++)
	{
		//array[i] = float( 1+ (ran);
		arr[i] =static_cast <int> (rand()) / static_cast <int> (RAND_MAX/2000) - 1000;  //create random numbers between 0 to 2000 and subtract 1000 to get -1000 to 1000
  }

  omp_set_num_threads(t); //set number of threads
  start = high_resolution_clock::now();
  msort(arr, n, ts);
  end = high_resolution_clock::now();

  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  cout << arr[0] << endl;
  cout << arr[n-1] << endl;
	cout << duration_sec.count() << endl;
}
