#include<iostream>
#include"convolution.h"
#include<ctime>


#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{
  int n = std::stoi(argv[1]);       //read command line arguments
  int t = std::stoi(argv[2]);

  high_resolution_clock::time_point start, end;
	duration<double , std::milli> duration_sec;

	srand(time(0));

	float* image = new float[n*n];
	float* mask = new float[9];      //mask size fixed to 3x3
  float* output = new float[n*n];

	for(int i = 0 ; i < n*n ; i ++)
	{
		//array[i] = float( 1+ (ran);
		image[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/20) - 10;   //assign random values

    if(i < 9)
    {
      mask[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5;
      //cout << "mask" << mask[i] << endl;
    }
    //cout << "image"<< image[i] << endl;
	}

  omp_set_num_threads(t);     //set number of threads
  start = high_resolution_clock::now();
	convolve(image, output, n , mask, 3 );
	end = high_resolution_clock::now();

	duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);

  cout << output[0] << endl;
	cout << output[n*n-1] << endl;
	cout << duration_sec.count() << endl;

  // for(int i =0 ;i < n ; i++)
  // {
  //   for(int j = 0 ; j<n ; j++)
  //   {
  //     printf("%f\t", output[i*n+j]);
  //   }
  //   printf("\n");
  // }

}
