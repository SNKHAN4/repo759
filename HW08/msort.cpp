#include<iostream>
#include <omp.h>
#include "msort.h"

using namespace std;

void printArr2(int *array, int size) {
   for(int i = 0; i<size; i++)
      std::cout << array[i] << "\t";
   std::cout << endl;
}

void serial_sort(int* arr, int left, int right , int n)   //insertion sort algorithm
{
  int i, j, temp;
  for(i = left+1 ; i <= right; i++)
  {
    j = i-1;
    temp = arr[i];
    while(j>=left && arr[j] > temp)
    {
      arr[j+1] = arr[j];
      j--;
    }
    arr[j+1] = temp;
  }
}

void sort_and_merge(int* arr, const std::size_t left, const std::size_t middle, const std::size_t right)
{
  int i , j , k , nleft, nright;
  nleft = middle-left+1;
  nright = right-middle;

  int* left_array= new int[nleft];
  int* right_array= new int[nright];

  //#pragma omp parallel for private(i) shared(left_array, arr)
  for( i = 0 ; i < nleft ; i++)
  {
    left_array[i] = arr[left + i];        //copy data into left array
  }


  //#pragma omp parallel for private(j) shared(right_array, arr)
  for( j = 0 ; j < nright ; j++)
  {
    right_array[j] = arr[middle + 1 + j];   //copy data into right array
  }

    i = 0;
    j = 0;
    k = left;


  while(i < nleft && j < nright)
  {
    if(left_array[i] <= right_array[j])       //compare elements of left and right array and assign to main array
    {
      arr[k] = left_array[i];
      i++;
    }
    else
    {
      arr[k] = right_array[j];
      j++;
    }
    k++;
  }

  while(i < nleft)        //left over elements
  {
    arr[k] = left_array[i];
    i++;
    k++;
  }
  while(j < nright)
  {
    arr[k] = right_array[j];
    j++;
    k++;
  }

  delete[] left_array;  //deallocate memrory
  delete[] right_array;

}

void recursive_split(int *array, int left, int right ,const std::size_t current_n,  const std::size_t n, const std::size_t threshold )
{
  //current_n is the size of the split array to be passed to the serial sort function and left right indicate the position of the elements to be sorted.
  if(current_n < threshold)
  {
    serial_sort(array, left, right , n);
  }
  else
  {
      if(left < right)
      {
          int middle = left+(right-left)/2;

          #pragma omp task
          recursive_split(array, left, middle, middle-left,n, threshold);

          #pragma omp task
          recursive_split(array, middle+1, right, right-(middle+1),n, threshold);

          //need to wait for both tasks to finish
          #pragma omp taskwait

          sort_and_merge(array, left, middle, right); //combine split arrays and sort
      }
   }
 }

void msort(int* arr, const std::size_t n, const std::size_t threshold)
{
    //std::cout << "merge sort" << endl;
    const std::size_t left = 0;
    const std::size_t right = n-1;
    //omp single because we want to launch the mergesort only once
    #pragma omp parallel
    {
      #pragma omp single
        recursive_split(arr, left, right , n, n, threshold);     //instead of splitting the array into parts and recursively calling mergesort function, created another function which just needs indices of the split arrays
    }
}
