#include <iostream>
#include "matmul.h"
#include <ctime>
#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{
  int n = std::stoi(argv[1]);   //read command line arguments
  int t = std::stoi(argv[2]);

  high_resolution_clock::time_point start, end;
	duration<double , std::milli> duration_sec;

  srand(time(0));

  float *A = (float*) malloc(n*n * sizeof(float));      //dynamically allocate arrays
	float *B = (float*) malloc(n*n * sizeof(float));
  float *C = (float*) malloc(n*n * sizeof(float));

  for(int i = 0 ; i < n*n ; i ++)
	{
		A[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;    //assign random values to the array elements
    B[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;
  }

  omp_set_num_threads(t);     //assign number of threads
  start = high_resolution_clock::now();
  mmul(A, B, C, n);
  end = high_resolution_clock::now();

  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  cout << C[0] << endl;
  cout << C[n*n-1] << endl;
  cout << duration_sec.count() <<  endl;

  // for(int i = 0 ; i < n ; i++)
  // {
  //   for(int j = 0 ; j < n ; j++)
  //   {
  //     printf("%f\t", C[i*n+j]);
  //   }
  //   printf("\n");
  // }


}
