#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <limits>
#include <cstdint>
#include <cinttypes>

__global__ void factorial()
{
    int i = threadIdx.x;
    std::printf("%d", i);
    int result;
    //cudaMalloc((void **)&result , sizeof(int));
    //result = i;
    while(i > 1)
    {
      result = result * (i-1);
      i--;
    }

    std::printf("!=%d\n" , result);


}

int main()
{
  std::printf("hello\n");
  const int numThreads = 8;
  factorial<<<1,8>>>();
  cudaDeviceSynchronize();
  std::printf("bye\n");
  return 0;
}
