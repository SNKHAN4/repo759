#include "reduce.h"
#include <chrono>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <cmath>



using namespace std;
using std::cout;
using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char **argv) {
    std::size_t n = std::stoi(argv[1]);   //read command line arguments
    std::size_t t = std::atoi(argv[2]);

    high_resolution_clock::time_point start;        //timer setup
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;
    
    srand(time(0));
    float *A = (float*) malloc(2*n * sizeof(float));      //dynamically allocate arrays
    for (std::size_t i = 0; i < 2*n; i++) {
        A[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;    //assign random values to the array elements
    }

    int myrank, nprocs;
    float global_res = 0, res = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    MPI_Barrier(MPI_COMM_WORLD); // synchronize the 2 processes to start at the same time
    
    if (myrank == 0) {
        start = high_resolution_clock::now();   //start timing on first call to reduce function
        omp_set_num_threads(t); // set number of threads from command line argument
        res = reduce(A, 0, n); //  process 0 performs reduction on first half of the array
    } else if (myrank == 1) {
        omp_set_num_threads(t); // set number of threads from command line argument
        res = reduce(A, n, 2 * n); // process 1 performs reduction on first half of the array
    }

    //combine both results into global_res
    MPI_Reduce(&res, &global_res, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    end = high_resolution_clock::now(); // end timer
    duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);

    if (myrank == 0) {
        cout << global_res << endl;
        cout << duration_sec.count() << endl;
    }
    
    MPI_Finalize(); 
    free(A);

    return 0;
}