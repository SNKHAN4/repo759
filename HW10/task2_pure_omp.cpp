#include "reduce.h"
#include <chrono>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <cmath>


using namespace std;
using std::cout;
using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char **argv) {
    size_t n = std::stoi(argv[1]);   //read command line arguments
    std::size_t t = std::atoi(argv[2]);

    high_resolution_clock::time_point start;        //timer setup
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;
    
    srand(time(0));
    float *A = (float*) malloc(n * sizeof(float));      //dynamically allocate arrays
    for (std::size_t i = 0; i < n; i++) {
        A[i] = static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;    //assign random values to the array elements
    }

    float global_res = 0;

    start = high_resolution_clock::now();   //start timing on first call to reduce function
    omp_set_num_threads(t); // set number of threads from command line argument
    global_res = reduce(A, 0, n);  // process 0 performs reduction on first half of the array

    //combine both results into global_res
    end = high_resolution_clock::now(); // end timer
    duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);

    cout <<   global_res << endl;
    cout <<   duration_sec.count() << endl;
    

    free(A);

    return 0;
}