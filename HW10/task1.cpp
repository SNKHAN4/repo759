#include <iostream>
#include "optimize.h"
#include <ctime>
#include <chrono>
#include <ratio>
#include <cmath>

using std::cout;
using std::chrono::high_resolution_clock;
using std::chrono::duration;

using namespace std;

int main(int argc , char *argv[])
{
  size_t n = std::stoi(argv[1]);   //read command line arguments
  high_resolution_clock::time_point start, end;
  duration<double , std::milli> duration_sec;

  vec v(n);

  v.data = (data_t*) malloc(n* sizeof(data_t));
  srand(time(0));
  //data_t *d = get_vec_start(v);
  for(size_t i = 0 ; i < n ; i ++)
	{
		v.data[i] = 1;//static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 ;    //assign random values to the array elements
    //std::cout << v.data[i] << std::endl; //<< "\t" << static_cast <double> (rand()) / static_cast <double> (RAND_MAX) - 0.5 << std::endl;
  }
  data_t dest;

  start = high_resolution_clock::now();
  optimize1(&v , &dest);
  end = high_resolution_clock::now();
  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  std::cout << dest << endl ; 
  std::cout << duration_sec.count() << std::endl ;

  start = high_resolution_clock::now();
  optimize2(&v , &dest);
  end = high_resolution_clock::now();
  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  std::cout << dest << endl ; 
  std::cout << duration_sec.count() << std::endl ;

  start = high_resolution_clock::now();
  optimize3(&v , &dest);
  end = high_resolution_clock::now();
  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  std::cout << dest << endl ; 
  std::cout << duration_sec.count() << std::endl ;

  start = high_resolution_clock::now();
  optimize4(&v , &dest);
  end = high_resolution_clock::now();
  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  std::cout << dest << endl ; 
  std::cout << duration_sec.count() << std::endl ;

  start = high_resolution_clock::now();
  optimize5(&v , &dest);
  end = high_resolution_clock::now();
  duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
  std::cout << dest << endl ; 
  std::cout << duration_sec.count() << std::endl ;

  return 0;
}
