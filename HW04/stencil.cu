#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "stencil.cuh"

using namespace std;

__global__ void stencil_kernel(const float* image, const float* mask, float* output, unsigned int n, unsigned int R)
{
    int g_thr_id =  blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int thr_id = threadIdx.x;
    int radius = R;   //converting radius value to signed int to get rid of warnings
    unsigned int mask_len = 2*R+1;
    unsigned int image_len = 2*R + blockDim.x;
    extern __shared__ float shared_mem[];   //shared memory array

    if(g_thr_id < n)    //avoid out of bounds memory access
    {
      if(thr_id < mask_len)     //load mask in inital threads of a block
      {
        shared_mem[thr_id] = mask[thr_id];
      }

      shared_mem[mask_len + thr_id +R ] = image[g_thr_id];  //load the n indices of image into shared memory

      if(threadIdx.x < R)
      {
        if( g_thr_id-radius > 0 )     //cases where the image has to be loaded into shared memory
        {
          shared_mem[mask_len + thr_id] = image[g_thr_id-R];
          //std::printf("shared_mem idx %d = im idx = %d by thread %d\n",mask_len + thr_id , g_thr_id-R, g_thr_id);
        }
        else
        {
          shared_mem[mask_len + thr_id] = 1.0;  //out of bounds for image so load 1
        }

        if(g_thr_id + mask_len < n-1)
        {
          shared_mem[mask_len + R + blockDim.x + thr_id] = image[g_thr_id + mask_len];
          //std::printf("shared_mem idx %d = im idx = %d by thread %d\n",mask_len + R + blockDim.x + thr_id ,g_thr_id + mask_len, g_thr_id);
        }
        else
        {
          shared_mem[mask_len + R + blockDim.x + thr_id] = 1.0;   //out of bounds for image so load 1
          //std::printf("shared_mem idx %d = 1 by thread %d\n",mask_len + R + blockDim.x + thr_id, g_thr_id );
        }
      }

      //image and mask loaded to shared memory, wait for all threads to finish
      __syncthreads();

      shared_mem[mask_len + image_len + thr_id] = 0;  //intiialize output to 0

      for(int j = 0 ; j < mask_len ; j++)   //-R to R range on j, but in positive numbers
      {
        shared_mem[mask_len + image_len + thr_id] += shared_mem[mask_len + thr_id + j] * shared_mem[j];
        // if(g_thr_id == 0)
        // {
        // std::printf("\n\ngthr_id = %d\t, thr_id = %d\no_shared_idx = %d\to_shared_val = %f\nshared_im_idx = %d\tvalue = %f\nshared_mask_idx = %d\tshared_mask_val= %f\n\n  ",
        //       g_thr_id,
        //       thr_id,
        //       mask_len + image_len + thr_id,
        //       shared_mem[mask_len + image_len + thr_id],
        //       mask_len + thr_id +j,
        //       shared_mem[mask_len + thr_id + j],
        //       j,
        //       shared_mem[j]
        //       );
        //     }
      }
      //need to finish all threads before writing to global memory
      __syncthreads();

      output[g_thr_id] = shared_mem[mask_len + image_len + thr_id];
    }
}

__host__ void stencil(const float* image,
                      const float* mask,
                      float* output,
                      unsigned int n,
                      unsigned int R,
                      unsigned int threads_per_block)
{

  //std::printf("numblocks = %d\n", (n+threads_per_block-1)/threads_per_block);
  stencil_kernel <<<(n+threads_per_block-1)/threads_per_block , threads_per_block, ((2 * R + 1) + (threads_per_block + 2 * R) + threads_per_block )*sizeof(float) >>> (image, mask, output, n , R);
  //cudaDeviceSynchronize();
  //std::printf("%f\n", C[n*n-1]);


}
