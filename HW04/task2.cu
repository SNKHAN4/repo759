#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "stencil.cuh"

using namespace std;
int main(int argc , char *argv[])
{
    unsigned int n = std::stoi(argv[1]);  //load command line arguments
    unsigned int R = std::stoi(argv[2]);
    unsigned int threads_per_block = std::stoi(argv[3]);

    //allocate host arrays
    float *image = (float*) malloc(n*sizeof(float));
    float *mask = (float*) malloc((2*R+1)*sizeof(float));
    float *output = (float*) malloc(n*sizeof(float));
    //std::printf("img:\t");

    //assign random numbers to host arrays
    for(unsigned int i = 0 ; i < n ; i++)
    {
      image[i] =static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
      //std::printf("%f\t", image[i]);
    }
    //std::printf("\nmask:\t");
    for(unsigned int i = 0 ; i < 2*R+1 ; i++)
    {
       mask[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
       //std::printf("%f\t", mask[i]);
    }
    //std::printf("\n");

    //allocate device arrays
      float *dImg = NULL;
      float *dMask = NULL;
      float *dOut = NULL;

      cudaMalloc((void **)&dImg , n*sizeof(float));
      cudaMalloc((void **)&dMask , (2*R+1)*sizeof(float));
      cudaMalloc((void **)&dOut , n*sizeof(float));

      //copy data from host array to device array
    cudaMemcpy(dImg, image , n*sizeof(float) , cudaMemcpyHostToDevice);
    cudaMemcpy(dMask, mask , (2*R+1)*sizeof(float) , cudaMemcpyHostToDevice);


    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);
    //start timer
    cudaEventRecord(start, 0);


    stencil(dImg,dMask, dOut, n, R, threads_per_block);

    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);

    cudaDeviceSynchronize();
    //copy output from device to host
    cudaMemcpy(output, dOut, n*sizeof(float), cudaMemcpyDeviceToHost);

    // for(unsigned int i = 0 ; i < n ; i++)
    // {
    //   std::printf("Output[%d] = %f\n", i , output[i]); //print elapsed timeTaken
    // }

    std::printf("%f\n",output[n-1]);
    std::printf("%f\n", timeTaken); //print elapsed timeTaken

    cudaEventDestroy(start);
    cudaEventDestroy(end);

    free(image);
    free(mask);
    free(output);

    cudaFree(dImg);
    cudaFree(dMask);
    cudaFree(dOut);
    return 0;
}
