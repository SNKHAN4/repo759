#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "matmul.cuh"

using namespace std;
int main(int argc , char *argv[])
{
    size_t n = std::stoi(argv[1]);      //reading n
    unsigned int threads_per_block = std::stoi(argv[2]);  //reading num_threads

    //allocate host arrays
    float *hA = (float*) malloc(n*n*sizeof(float));
    float *hB = (float*) malloc(n*n*sizeof(float));
    float *hC = (float*) malloc(n*n*sizeof(float));

    srand(0); //new seed on each run
    //fill random values between -1 and 1
    for(unsigned i = 1 ; i <= n*n ; i++)
    {
      hA[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
      hB[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
    }

    //allocate device arrays
    float *dA = NULL;
    float *dB = NULL;
    float *dC = NULL;

    cudaMalloc((void **)&dA , n*n*sizeof(float));
    cudaMalloc((void **)&dB , n*n*sizeof(float));
    cudaMalloc((void **)&dC , n*n*sizeof(float));

    //copy host array data to device memory
    cudaMemcpy(dA, hA, n*n*sizeof(float) , cudaMemcpyHostToDevice);   //copy host data to device
    cudaMemcpy(dB, hB, n*n*sizeof(float) , cudaMemcpyHostToDevice);

    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);
    //start timer
    cudaEventRecord(start, 0);

    matmul(dA,dA,dC,n,threads_per_block);

    //end timer
    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);

    cudaDeviceSynchronize();
    cudaMemcpy(hC, dC, n*n*sizeof(float), cudaMemcpyDeviceToHost);

    std::printf("%f\n", hC[n*n-1]); //print last element
    std::printf("%f\n", timeTaken); //print elapsed timeTaken

    cudaEventDestroy(start);
    cudaEventDestroy(end);

    //de-allocate memory
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);

    free(hA);
    free(hB);
    free(hC);

    return 0;
}
