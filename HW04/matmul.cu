#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

#include "matmul.cuh"

using namespace std;
__global__ void matmul_kernel(const float* A, const float* B, float* C, size_t n)
{
    int index = (blockIdx.x *blockDim.x)  + threadIdx.x;    //global index of thread

    if(index < n*n)     //to avoid out of bounds access
    {
      int i = 0;
      if(index %n == 0)   //if thread id is a multiple of n, row = thread id
          i = index;
      else
      {
        i = (index/n) * n;  //if thread id is not a multiple of n, row = closest multiple of n to thread id
      }
      C[index] = 0;
      for(int j = 0 ; j < n ; j++)
      {
        int b_index = index%n;
        C[index] += A[i + j]*B[b_index + j*n];  //matmul operation
      }

    }
}

void matmul(const float* A, const float* B, float* C, size_t n, unsigned int threads_per_block)
{
    //calling kernel
    matmul_kernel<<<(n*n+threads_per_block-1)/threads_per_block,threads_per_block>>>(A, B, C, n);
}
