#include "scan.cuh"

#include <cuda.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>

using namespace std;

__global__ void scan_hillis_steele(float*g_odata, const float*g_idata, int n, float *aux_data, int aux_bool)  //taken from lecture slides
{
  extern volatile __shared__ float temp[]; // allocated on invocation
  int thid= threadIdx.x;
  int pout = 0, pin = 1;
  int global_index = blockIdx.x * blockDim.x + threadIdx.x;

  // load input into shared memory. // **exclusive** scan: shift right by one element and set first output to 0

  if(global_index < n)
  {
    temp[thid] = g_idata[global_index];
    __syncthreads();

    for (int offset = 1; offset < blockDim.x; offset *= 2)
    {
        pout = 1 - pout; // swap double buffer indices
        pin = 1 - pout;

        if (thid >= offset)
            temp[pout * blockDim.x + thid] = temp[pin * blockDim.x + thid] + temp[pin * blockDim.x + thid - offset];  //modified for multiple blocks
        else
            temp[pout * blockDim.x + thid] = temp[pin * blockDim.x + thid];

        __syncthreads();
      }

        g_odata[global_index] = temp[pout * blockDim.x + thid];

        if (aux_bool == 1) {    //only for first kernel call
            if (thid == blockDim.x - 1) {
                aux_data[blockIdx.x] = temp[pout * blockDim.x + thid];
            }
        }
  }
  }

__global__ void accumulate(float *data_array, float *scan_array, int n)      //function to
{

      int global_idx = blockIdx.x * blockDim.x + threadIdx.x;
      if (global_idx < n)
        data_array[global_idx] =   data_array[global_idx] + scan_array[blockIdx.x] ;    //accumulate

}

  __host__ void scan(const float *input, float *output, unsigned int n, unsigned int threads_per_block)
  {
    int numBlocks = (n+threads_per_block-1)/ (threads_per_block);
    float *last_elements, *scan_array, *temp;
    cudaMallocManaged(&last_elements, numBlocks * sizeof(float));     // store last element of each block
    cudaMallocManaged(&scan_array, numBlocks * sizeof(float));        // array for storing scan outputs
    cudaMallocManaged(&temp, numBlocks * sizeof(float));              //temporary array to pass to kernel

    int shared_mem_size = 2*threads_per_block*sizeof(float);

    scan_hillis_steele<<<numBlocks, threads_per_block, shared_mem_size>>>(output, input, n, last_elements, 1);
    cudaDeviceSynchronize();
    scan_hillis_steele<<<1, threads_per_block, shared_mem_size>>>(scan_array, last_elements, numBlocks, temp, 0);
    cudaDeviceSynchronize();
    if(numBlocks > 1)
    {
      accumulate<<<numBlocks, threads_per_block>>>(output, scan_array, n);
    }

}
