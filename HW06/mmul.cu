#include "mmul.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
void mmul(cublasHandle_t handle, const float* A, const float* B, float* C, int n)
{
   const float alf = 1;
   const float *alpha = &alf;   //getting type errors if 1 passed directly to cublas function call
  //cublas errors
  cublasSgemm(handle,
                         CUBLAS_OP_N, CUBLAS_OP_N,
                         n, n, n,
                         alpha,
                         A, n,
                         B, n,
                         alpha,
                         C, n
                       );

}
