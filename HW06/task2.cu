#include "scan.cuh"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
#include <cstdio>
#include <iostream>

using namespace std;

int main(int argc , char *argv[])
{
  size_t n = std::stoi(argv[1]);                       //reading n
  size_t threads_per_block = std::stoi(argv[2]);      //reading threads per block

  //allocating managed memory for A and B
  float *A, *B;

  cudaMallocManaged(&A, n*sizeof(float));
  cudaMallocManaged(&B, n*sizeof(float));

  srand(0); //new seed on each run
  for(unsigned i = 1 ; i <= n ; i++)
  {
    A[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
  }

    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);
    //start timer
    cudaEventRecord(start, 0);

    scan(A, B, n, threads_per_block);     //call kernel

    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);

    cudaDeviceSynchronize();

    std::printf("%f\n" ,B[n-1]);
    std::printf("%f\n", timeTaken);

    cudaFree(A);
    cudaFree(B);

    return 0;
}
