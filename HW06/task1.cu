#include "mmul.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

#include <cuda.h>
#include <cstdio>
#include <iostream>

using namespace std;

int main(int argc , char *argv[])
{
  size_t n = std::stoi(argv[1]);            //reading n
  size_t n_tests = std::stoi(argv[2]);      //reading n_tests

  //allocating managed memory for A, B and C
  float *A, *B, *C;

  cudaMallocManaged(&A, n*n*sizeof(float));
  cudaMallocManaged(&B, n*n*sizeof(float));
  cudaMallocManaged(&C, n*n*sizeof(float));

  srand(0); //new seed on each run
  //store random values from -1 to 1 into input arrays.
  for(unsigned i = 1 ; i <= n*n ; i++)
  {
    A[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
    B[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
    C[i-1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/2) - 1;
  }

  cublasStatus_t err_stat;
  cublasHandle_t handle;    //handle for cublas object

  float time_sum = 0;       //initialize sum of execution times to zero
  cublasCreate(&handle);    //create instance of cublas handle
  for(int i = 0 ; i < n_tests ; i++)  //run kernel n test times
  {
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);
    //start timer
    cudaEventRecord(start, 0);

    mmul(handle, A, B , C, n);

    cudaEventRecord(end, 0);  //only record how much time it took to execute this on gpu

    cudaEventSynchronize(end);
    float timeTaken;
    cudaEventElapsedTime(&timeTaken, start, end);
    time_sum = time_sum+timeTaken;

  }
  cudaDeviceSynchronize();

  std::printf("%f\n", time_sum/n_tests);

}
