#include<iostream>
#include<string>

using namespace std;

int main(int argc, char *argv[] )
{
	if(argc == 2)
	{
		//std::cout << "number of arguments:\t" << argc << "\n";
		int N = std::stoi(argv[1]);
		//std::cout << "value entered:\t" << N << "\n";
		int i;
		for( i = 0 ; i <= N ; i++)
			printf("%d ", i);
		printf("\n");
			
		for(i = N ; i>= 0 ; i--)
			std::cout << i << " " ;
		std::cout<<endl;
	}
	else 
		std::cout << "Incorrect number of command line arguments entered\n";
	
	return(0);
}

